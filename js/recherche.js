/**
 * Script pour la recherche d'un produit
 */

 //Juste pour dire à JSHINT que les fonctions existent
 /*global utilAjax*/
 /*global utilDom*/
 /*global scriptSug*/


"use strict";


var scRecherche = {
    "txtBoxRecherche" : null,
    "rechercheEffectue" : false,
    "scrolling"    : false,
    "spErreur"     : null,
    "clientHttp"   : null,
    "clientHttpSugg": null,
    "divCont"      : null,
    "strDerniereRecherche": null,
    "optionsTri": null,
    "offset": null,


    afficherMessageDansResultatRecherche : function (paramMessage)
    {
        //<br>
        var saut;
            for (var index = 0; index < 5; index++) {
                saut = document.createElement("BR");
                document.getElementById("contenantDeContenantRech").appendChild(saut);
            }
            //<h3>
            var titreMessage = document.createElement("H3");
            var message = document.createTextNode(paramMessage);
            document.getElementById("contenantDeContenantRech").appendChild(titreMessage);
            titreMessage.appendChild(message);
    },

    /**
     * Affiche les produits
     */
    afficherProduit: function (reponse) 
    {
        //fonction appelée par l'event scroll
        if (!scRecherche.scroll) 
        {   
            utilDom.viderElement(scRecherche.divCont);
            if (reponse[0].trouve == false) 
            {
                scRecherche.afficherMessageDansResultatRecherche("Votre recherche n'a retourné aucun résultat");
            }
        }
        scRecherche.scroll = false;
        if (reponse[0].trouve == true) 
        {
            scRecherche.rechercheEffectue = true;
            scRecherche.divCont.hidden = false;
            var contenuRecherche;
            var lienProduit;
            var imgRech;
            var titrePanier;
            var prix;
            var description;
            for (var i = 1; i < reponse.length; i++) 
            {
            //Création des nodes
                //div
                contenuRecherche = document.createElement("DIV");
                contenuRecherche.className = "contenuRecherche";
                //a
                lienProduit = document.createElement("A");
                lienProduit.href = "produit-detail.php/?id="+reponse[i].id;
                //img
                imgRech = document.createElement("IMG");
                imgRech.className = "imgRech";
                imgRech.src = "/images/placeholder.png";
                //h3
                titrePanier = document.createElement("H3");
                titrePanier.className = "titrePanier";
                titrePanier.appendChild(document.createTextNode(reponse[i].titre));

            //Mise en place des conteneurs
                scRecherche.divCont.appendChild(contenuRecherche);
                contenuRecherche.appendChild(lienProduit);
                lienProduit.appendChild(imgRech);
                lienProduit.appendChild(titrePanier);

            //Prix + description
                //br
                lienProduit.appendChild(document.createElement("BR"));
                //prix
                prix = document.createTextNode("Prix = " + reponse[i].prix + "$");
                contenuRecherche.appendChild(prix);
                //<br><br>
                contenuRecherche.appendChild(document.createElement("BR"));
                contenuRecherche.appendChild(document.createElement("BR"));
                //description
                description = document.createTextNode(reponse[i].description);
                contenuRecherche.appendChild(description);
            }
        }    
    },

    /**
     * Fonction appelée lorsque la réponse AJAX revient.
     */
    gererRetourRecherche: function ()
    {

        if (scRecherche.clientHttp.status === 200) {
           try {
                var reponse = JSON.parse(scRecherche.clientHttp.responseText);
                scRecherche.afficherProduit(reponse);
            } catch (e) {
                window.console.error(
                    "La réponse AJAX n\'est pas une expression JSON valide.");
                window.console.error(e.message);
                window.console.error(scRecherche.clientHttp.responseText);
            } 
        } else {
            window.console.error(
                scRecherche.clientHttp.status + 
                " : " + scRecherche.clientHttp.responseText);
        }
        scRecherche.clientHttp = null;
    },

    //retourne l'option de tri séléctionnée
    getOptionTri: function ()
    {
        var retour;
        if (document.getElementById("asc").checked) {
            retour = "asc";
        }
        else if(document.getElementById("desc").checked)
        {
            retour =  "desc";
        }
        else retour =  "aucun";
        scRecherche.optionsTri = retour;
        return retour;
    },

    /**
     * Fonction appelée pour tenter de récupérer et d'afficher les informations
     * d'une produit avec AJAX.
     */
    rechercherProduit: function () 
    {
        var strChercher = scRecherche.txtBoxRecherche.value;
        document.getElementById("divSuggestions").hidden = true;
        if (scRecherche.clientHttp != null) {
        // Annuler la requête précédente car on lancera une nouvelle requête
        // à chaque input et on ne veut plus le résultat de la requête précédente.
        scRecherche.clientHttp.abort();
        }

        scRecherche.clientHttp = new XMLHttpRequest();
        scRecherche.rechercheEffectue = false;
        if(!scRecherche.scroll)
        {
            scRecherche.offset = 0;
        }

        if (scRecherche.txtBoxRecherche.value != scRecherche.strDerniereRecherche || scRecherche.optionsTri != scRecherche.getOptionTri())
        {
            utilDom.viderElement(scRecherche.divCont);

            if (scRecherche.txtBoxRecherche.value != "") 
            {
                scRecherche.afficherMessageDansResultatRecherche("Recherche en cours...");
                var parametres = {
                    "recherche" : strChercher,
                    "order"   : scRecherche.getOptionTri(),
                    "offset" : scRecherche.offset
                };
                utilAjax.envoyerRequeteAjax(
                    scRecherche.clientHttp,
                    "../ajax/recherche-produit.php",
                    "GET",
                    parametres,
                    scRecherche.gererRetourRecherche
                );
            }
        }
        scRecherche.strDerniereRecherche = scRecherche.txtBoxRecherche.value;
    },

    //Gère quand afficher les suggestions
    gererAffichageSuggestions: function ()
    {
        var strChercher = scRecherche.txtBoxRecherche.value;
        if (strChercher.length > 2) 
        {
            scRecherche.clientHttpSugg = new XMLHttpRequest();
            var parametres = {
                "suggestion" : strChercher
            };
            utilAjax.envoyerRequeteAjax(
                scRecherche.clientHttpSugg,
                "../ajax/suggestion.php",
                "GET",
                parametres,
                scriptSug.afficherSuggestions
            );

        }
        else
        {
            utilDom.viderElement(document.getElementById("divSuggestions"));
            document.getElementById("divSuggestions").hidden = true;
        }
        if (scRecherche.clientHttp != null) {
            // Annuler la requête précédente car on lancera une nouvelle requête
            // à chaque input et on ne veut plus le résultat de la requête précédente.
            scRecherche.clientHttp.abort();
        }
    },

    /*=============DÉFILEMENT INFINI==============*/
    gererDefilement : function() 
    {
        //si on est à 90% de la page on load les prochains 10
        if((window.innerHeight + window.scrollY) >= 0.75*document.body.offsetHeight &&
         scRecherche.rechercheEffectue && scRecherche.clientHttp == null)
        {
            scRecherche.scroll = true;
            scRecherche.offset+=10;
            var parametres = {
                "recherche" : scRecherche.strDerniereRecherche,
                "order"   : scRecherche.getOptionTri(),
                "offset" : scRecherche.offset
            };
            scRecherche.clientHttp = new XMLHttpRequest();
            utilAjax.envoyerRequeteAjax(
                scRecherche.clientHttp,
                "../ajax/recherche-produit.php",
                "GET",
                parametres,
                scRecherche.gererRetourRecherche
            );
        }
    },

    /**
     * Appelée lors de l'initialisation de la page
     */
    initialisation : function() 
    {
        //Variables
        scRecherche.txtBoxRecherche = document.getElementById("txtRecherche");
        scRecherche.divCont = document.getElementById("contenantDeContenantRech");
        scRecherche.divCont.hidden = false;
        scRecherche.offset = 0;

        //Events
        document.getElementsByClassName("recherche")[0].addEventListener("submit",scRecherche.rechercherProduit);
        scRecherche.txtBoxRecherche.addEventListener("input", scRecherche.gererAffichageSuggestions, false);
        // Événement "scroll" appelé lors du défilement de la fenêtre.
        window.addEventListener('scroll', scRecherche.gererDefilement, false);

        //RadioBoxes
        document.getElementById("asc").addEventListener("change", scRecherche.rechercherProduit, false);
        document.getElementById("desc").addEventListener("change", scRecherche.rechercherProduit, false);
        document.getElementById("aucun").addEventListener("change", scRecherche.rechercherProduit, false);

    }
};
window.addEventListener("load", scRecherche.initialisation, false);