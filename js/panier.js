/**
 * Script pour l'affichage du panier dans le header
 */
/*global utilCookies*/
/*global utilAjax*/
/*global utilDom*/

"use strict";

var scPanier =
{
    "strCookiePanier"   : null,
    "elemsPanier"       : null,
    "logoPanier"        : null,
    "infoPanier"       : null,
    "divPanier"         : null,
    "clientHttp"        : null,

    GetNbElem : function ()
    {
        if (scPanier.strCookiePanier==null || scPanier.strCookiePanier.indexOf('.') == -1) 
        {
            return 0;
        }
        else
        {
            return document.cookie.split(".").length - 1;
        }
    },

    //synchronise scPanier.elemsPanier avec le cookie
    SetElemsPanier : function ()
    {
        scPanier.strCookiePanier = utilCookies.GetValeurCookie("panier");
        if (scPanier.GetNbElem() > 0) 
        {
            scPanier.elemsPanier = scPanier.strCookiePanier.split('.');
            for (var i=0; i < scPanier.elemsPanier.length-1; i++) 
            {
                scPanier.elemsPanier[i] = scPanier.elemsPanier[i].split('=');
            }
            scPanier.elemsPanier.pop();
        }
    },
    EstDansListe: function (elem)
    {
        return scPanier.strCookiePanier.match('(.|^)'+elem+'(=)');
    },

    RetirerDuPanier : function (idProduit)
    {
        if(scPanier.EstDansListe(idProduit))
        {
            var retour = '';
            var lesProduits = scPanier.elemsPanier;
            for (var i=0; i < lesProduits.length; i++)
            { 
                //trouve produit
                if (lesProduits[i][0] != idProduit)
                {
                    retour += lesProduits[i][0] + '=' + lesProduits[i][1] + '.';
                }
            }
            utilCookies.SetCookie("panier", retour);
            scPanier.SetElemsPanier();

             //Traitement de l'affichage
            document.getElementById("panier"+idProduit).remove();
            var titre = document.getElementById("panierTitre");
            utilDom.viderElement(titre);
            titre.appendChild(document.createTextNode("Mon Panier ("+scPanier.GetNbElem()+")"));
            //update le chiffre du panier
            utilDom.viderElement(document.getElementById("nbLogoPanier"));
            document.getElementById("nbLogoPanier").appendChild(document.createTextNode(scPanier.GetNbElem()));
            
        }
    },

    //(id peut être -1)
    AjouterQteProduit: function (id, qte)
    {
        var retour = '';
        var lesProduits = scPanier.elemsPanier;
        var nouvelleQte = 0;
        for (var i=0; i < lesProduits.length; i++)
        { 
            //produit pas trouvé
            if (lesProduits[i][0] != id)
            {
                retour += lesProduits[i][0] + '=' + lesProduits[i][1] + '.';
            }
            //produit trouvé
            else 
            {
                nouvelleQte = parseInt(lesProduits[i][1]) + qte;
                var max = null;
                if (nouvelleQte > this.infoPanier[i].quantite) 
                {
                    nouvelleQte = this.infoPanier[i].quantite;
                    max = document.createElement("P");
                    max.appendChild(document.createTextNode("Quantité max atteinte!"));
                    max.style.color = "red";
                    max.style.margin = 0;
                    max.style.display = "inline-block";
                    max.style.paddingLeft = "5px";
                }
                if (nouvelleQte > 0) 
                {
                    retour += lesProduits[i][0] + '=' + nouvelleQte + '.';
                }
                var qteDiv = document.getElementById("qte" + id);
                utilDom.viderElement(qteDiv);
                qteDiv.appendChild(document.createTextNode("Quantité: " + nouvelleQte + " (En stock: " + this.infoPanier[i].quantite + ")"));
                if (max) {
                    qteDiv.appendChild(max);
                }
            }
        }
        utilCookies.SetCookie("panier", retour);
        scPanier.SetElemsPanier();

        //Traitement de l'affichage
        if (!scPanier.EstDansListe(id)) 
        {
            document.getElementById("panier"+id).remove();
            var titre = document.getElementById("panierTitre");
            utilDom.viderElement(titre);
            titre.appendChild(document.createTextNode("Mon Panier ("+scPanier.GetNbElem()+")"));
            //update le chiffre du panier
            utilDom.viderElement(document.getElementById("nbLogoPanier"));
            document.getElementById("nbLogoPanier").appendChild(document.createTextNode(scPanier.GetNbElem()));
        }
    },

    AjoutEventBoutonQte : function(balise, id, qte)
    {
        balise.addEventListener("click", function() {scPanier.AjouterQteProduit(id, qte);});
        if (qte == null) 
        {
            balise.addEventListener("click", function() {scPanier.RetirerDuPanier(id);});
        }
    },


    //loads les divs des produits
    PreparerAffichagePanier: function ()
    {
        //Message "Mon Panier"
        var monPanierTitre = document.createElement("h2");
        monPanierTitre.id = "panierTitre";
        monPanierTitre.style.margin = "5px";
        monPanierTitre.style.marginLeft = 0;
        monPanierTitre.appendChild(document.createTextNode("Mon panier"));
        scPanier.divPanier.appendChild(monPanierTitre);
        monPanierTitre.appendChild(document.createTextNode(" ("+this.GetNbElem()+")"));

        //Boucle de chaque produits
        if (this.strCookiePanier != "") 
        {
            var total = 0;
            for (var i = 0; i < scPanier.infoPanier.length; i++) 
            {
                //DIV
                var divGeneral = document.createElement("DIV");
                divGeneral.id = "panier"+scPanier.infoPanier[i].idProduit;
                divGeneral.className = "divMenuPanier";
                scPanier.divPanier.appendChild(divGeneral);

                //Lien
                var lienProd = document.createElement("A");
                lienProd.href = "/sources/produit-detail.php/?id="+scPanier.infoPanier[i].idProduit;
                divGeneral.appendChild(lienProd);
    
                //IMG
                var imgProduit = document.createElement("IMG");
                imgProduit.src = "/images/placeholder.png";
                imgProduit.className = "imgMenuPanier";
                lienProd.appendChild(imgProduit);
    
                //titre
                var titre = document.createElement("H3");
                titre.className = "titreMenuPanier";
                titre.appendChild(document.createTextNode(scPanier.infoPanier[i].titre));
                lienProd.appendChild(titre);
                divGeneral.appendChild(document.createElement("BR"));

                //Prix
                var prixTitre = document.createElement("H4");
                prixTitre.className = "titreMenuPanier";
                prixTitre.appendChild(document.createTextNode("Prix: " +scPanier.infoPanier[i].prix + " $"));
                divGeneral.appendChild(prixTitre);
                divGeneral.appendChild(document.createElement("BR"));

                //Quantité
                var qteTitre = document.createElement("H4");
                qteTitre.className = "titreMenuPanier";
                qteTitre.id = "qte" + scPanier.infoPanier[i].idProduit;
                qteTitre.appendChild(document.createTextNode("Quantité: " +scPanier.elemsPanier[i][1] + " (En stock: " + this.infoPanier[i].quantite + ")"));
                divGeneral.appendChild(qteTitre);
          

                //Boutons
                var divBtns = document.createElement("DIV");
                divGeneral.appendChild(divBtns);
                //-
                var btnMoins = document.createElement("BUTTON");
                btnMoins.appendChild(document.createTextNode("-"));
                scPanier.AjoutEventBoutonQte(btnMoins, scPanier.infoPanier[i].idProduit, -1);
                divBtns.appendChild(btnMoins);
                //+
                var btnPlus = document.createElement("BUTTON");
                btnPlus.appendChild(document.createTextNode("+"));
                scPanier.AjoutEventBoutonQte(btnPlus, scPanier.infoPanier[i].idProduit, 1);
                divBtns.appendChild(btnPlus);

                //Enlever du panier
                var enlever = document.createElement("P");
                enlever.className = "enlever";
                enlever.appendChild(document.createTextNode("Retirer du panier"));
                scPanier.AjoutEventBoutonQte(enlever, scPanier.infoPanier[i].idProduit, null);
                divGeneral.appendChild(enlever);
            }
            var retour = document.createElement("A");
            retour.className = "retourPanier";
            retour.href = "/sources/mon-panier.php";
            retour.appendChild(document.createTextNode("Aller au panier"));
            this.divPanier.appendChild(retour);
        }
        //Aucun produit présent
        else    
        {
            var msg = document.createElement("h3");
            msg.appendChild(document.createTextNode("Aucun produit séléctionné"));
            var lien = document.createElement("A");
            lien.href = "/sources/recherche.php";
            lien.appendChild(document.createTextNode("Aller à la recherche d'un produit"));
            scPanier.divPanier.appendChild(msg);
            msg.appendChild(document.createElement("BR"));
            msg.appendChild(document.createElement("BR"));
            msg.appendChild(lien);
        }
    },
    
    ///Gère le retour du JSON
    InitialiserAjaxOutPanier: function ()
    {
        if (scPanier.clientHttp.status === 200) {
            try {
                 var reponse = JSON.parse(scPanier.clientHttp.responseText);
                 scPanier.infoPanier = reponse;
             } catch (e) {
                 window.console.error(
                     "La réponse AJAX n\'est pas une expression JSON valide.");
                 window.console.error(e.message);
                 window.console.error(scPanier.clientHttp.responseText);
             }
             scPanier.SetElemsPanier();
             scPanier.PreparerAffichagePanier();
         } else {
             window.console.error(
                scPanier.clientHttp.status + 
                 " : " + scPanier.clientHttp.responseText);
         }
         scPanier.clientHttp = null;
    },

    //Initialise et attribue la variable lesProduitsPop
    InitialiserAjaxPanier: function ()
    {
        scPanier.clientHttp = new XMLHttpRequest();
        var parametres = {
            "Intialisation" : true
        };
        utilAjax.envoyerRequeteAjax(
            scPanier.clientHttp,
            "/ajax/panier.php",
            "GET",
            parametres,
            scPanier.InitialiserAjaxOutPanier
        );
    },

    AffichagePanier: function (e)
    {
        if  ((document.getElementById("pardessusPanier").contains(e.target) && scPanier.divPanier.hidden|| 
        scPanier.divPanier.contains(e.target))) 
        {
            scPanier.divPanier.hidden = false;
            scPanier.divPanier.style.display = "flex";
        }
        else 
        {
            scPanier.divPanier.style.display = "none";
            scPanier.divPanier.hidden = true;
        }
    },

    initialisation: function ()
    {
        //Variables
        scPanier.logoPanier = document.getElementById("panier"); 
        scPanier.logoPanier.hidden = false;
        scPanier.divPanier = document.getElementById("divMenuPanier");
        scPanier.divPanier.style.display = "none";

        //Cookies
        scPanier.strCookiePanier = utilCookies.GetValeurCookie("panier");
        if (scPanier.strCookiePanier == null)
        {
            utilCookies.SetCookie("panier", "");
            scPanier.strCookiePanier = "";
        }
        scPanier.SetElemsPanier();
        scPanier.InitialiserAjaxPanier();

        //Events
        window.addEventListener('click', function(e){scPanier.AffichagePanier(e);}, true);
        document.getElementById("logoPanier").style.borderRadius = "30px";
        document.getElementById("pardessusPanier").addEventListener("mouseover", 
        function() {document.getElementById("logoPanier").style.backgroundColor = "ivory";});
        document.getElementById("pardessusPanier").addEventListener("mouseout", 
        function() {document.getElementById("logoPanier").style.backgroundColor = "transparent";});

    }
};

scPanier.initialisation();

