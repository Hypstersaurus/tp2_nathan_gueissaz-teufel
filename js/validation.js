"use strict";

/*global utilAjax*/
/*global utilDom*/

var scVal = 
{
    "txtBoxCourriel"    : null,
    "form"              : null,
    "txtBoxNom"         : null,
    "txtBoxMail"        : null,
    "txtBoxMdpA"        : null,
    "txtBoxMdpB"        : null,
    "txtBoxAdresse"        : null,
    "msgNom"            : null,
    "msgMail"           : null,
    "msgMdpA"             : null,
    "msgMdpB"             : null,
    "msgAdresse"             : null,
    "clientHttp"        : null,
    "nomValide"         : false,
    "mdpValide"         : false,
    "mailValide"        : false,
    "adresseValide"        : false,
    "eventNom"             :null,
    "eventAdresse"             :null,
    "eventCourriel"             :null,
    "eventMdpA"             :null,
    "eventMdpB"             :null,

    VerifEmailOut : function ()
    {
        if (scVal.clientHttp.status === 200) {
            try {
                 var reponse = JSON.parse(scVal.clientHttp.responseText);
                 if (reponse == "L'adresse courriel est valide") 
                 {
                    scVal.AfficherMsg(scVal.msgMail, reponse, "green");
                    scVal.mailValide = true;
                 }
                 else
                 {
                    scVal.AfficherMsg(scVal.msgMail, reponse, "red");
                    scVal.mailValide = false;
                 }
             } catch (error) {
                 window.console.error(
                     "La réponse AJAX n\'est pas une expression JSON valide.");
                 window.console.error(error.message);
                 window.console.error(scVal.clientHttp.responseText);
             }
         } else {
             window.console.error(
                scVal.clientHttp.status + 
                 " : " + scVal.clientHttp.responseText);
         }
         scVal.clientHttp = null;
        //Event de fin
        if (scVal.mailValide) 
        {
            scVal.eventCourriel = scVal.txtBoxCourriel.addEventListener("input", scVal.VerifEmail);
        }
        else if (scVal.eventCourriel != null)
        {
            scVal.eventCourriel = scVal.txtBoxCourriel.removeEventListener("input", scVal.VerifEmail);
        }
    },

    VerifEmail : function ()
    {
        scVal.txtBoxCourriel.value = scVal.txtBoxCourriel.value.trim();
        if (scVal.txtBoxCourriel.value == "") 
        {
            scVal.msgMail.hidden = true;
        }
        else
        {
            scVal.AfficherMsg(scVal.msgMail, "Adresse en traitement...", "black");
            if (scVal.txtBoxCourriel.value.length < 50 && scVal.txtBoxCourriel.value.match(new RegExp(/\S+@\S+\.\S+/gm))) 
            {
                scVal.clientHttp = new XMLHttpRequest();
                var params = {"mail" : scVal.txtBoxCourriel.value};
                utilAjax.envoyerRequeteAjax(
                    scVal.clientHttp,
                    "../ajax/validation.php",
                    "GET",
                    params,
                    scVal.VerifEmailOut
                );   
            }
            else
            {
                scVal.AfficherMsg(scVal.msgMail,"\""+ scVal.txtBoxCourriel.value + 
                "\" n'est pas une adresse courriel valide (example@example.com en dessous de 50 caractères)", "red");
                //Event de fin 
                if (scVal.eventCourriel != null)
                {
                    scVal.eventCourriel = scVal.txtBoxCourriel.removeEventListener("input", scVal.VerifEmail);
                }
            }
        }
    },

    Authentification : function(e, r)
    {
        if (scVal.txtBoxMdpB.hidden == true)
        {
            scVal.VerifMdpA();
        }
        if (scVal.clientHttp == null) 
        {
            //on revérifie (si les champs ont étés remplis automatiquement) r montre si la revérification a déjà été faite
            if (!r && !(scVal.mdpValide && scVal.mailValide && scVal.nomValide && scVal.adresseValide)) 
            {
                scVal.VerifEmail();
                scVal.VerifAdresse();
                scVal.VerifNom();
                scVal.VerifMdpA();
                scVal.VerifMdpB();
                scVal.Authentification(e,true);
            }
            //vérification invalide et revérification déjà faite
            else if(r)
            {
                e.preventDefault();
            }
        }
        //email encore en traitement
        else
        {
            e.preventDefault();
        }
    },

    AfficherMsg : function (balise, msg, color)
    {
        utilDom.viderElement(balise);
        balise.appendChild(document.createTextNode(msg));
        balise.style.color = color;
        balise.style.fontWeight = "bold";
        balise.hidden = false;
    },

    VerifMdpA : function ()
    {
        if (scVal.txtBoxMdpA.value == "") 
        {
            utilDom.viderElement(scVal.msgMdpA);
            utilDom.viderElement(scVal.msgMdpB);
            scVal.mdpValide = false;
            scVal.txtBoxMdpB.hidden = true;
            
        }
        else
        {
            scVal.mdpValide = scVal.txtBoxMdpA.value.length > 3;
            if  (scVal.mdpValide)
            {
                scVal.AfficherMsg(scVal.msgMdpA,"Mot de passe valide", "green");
                scVal.mdpValide = true;
                scVal.txtBoxMdpB.hidden = false;
            }
            else
            {
                scVal.AfficherMsg(scVal.msgMdpA,"Mot de passe invalide (4 caractères et plus)", "red");
                scVal.mdpValide = false;
                scVal.txtBoxMdpB.hidden = true;
            }
        }
        //Event de fin
        if (scVal.mdpValide) 
        {
            scVal.eventMdpA = scVal.txtBoxMdpA.addEventListener("input", scVal.VerifMdpA);
        }
        else if (scVal.eventMdpA != null)
        {
            scVal.eventMdpA = scVal.txtBoxMdpA.removeEventListener("input", scVal.VerifMdpA);
        }
        scVal.VerifMdpB();
    },

    VerifMdpB : function ()
    {
        if (scVal.txtBoxMdpB.value == "") 
        {
            utilDom.viderElement(scVal.msgMdpB);
            scVal.mdpValide = false;
            scVal.msgMdpB.hidden = true;
        }
        else
        {
            scVal.mdpBValide = scVal.txtBoxMdpA.value == scVal.txtBoxMdpB.value;
            if (scVal.mdpBValide) 
            {
                scVal.AfficherMsg(scVal.msgMdpB,"Les mots de passe correspondent", "green");
                scVal.mdpValide = true;
            }
            else 
            {
                scVal.AfficherMsg(scVal.msgMdpB,"Les mots de passe ne correspondent pas", "red");
                scVal.mdpValide = false;
            }
        }
        //Event de fin
        if (scVal.msgMdpB.style.color == "green") 
        {
            scVal.eventMdpB = scVal.txtBoxMdpB.addEventListener("input", scVal.VerifMdpB);
        }
        else if (scVal.eventMdpB != null)
        {
            scVal.eventMdpB = scVal.txtBoxMdpB.removeEventListener("input", scVal.VerifMdpB);
        }
    },

    VerifNom : function ()
    {
        var val = scVal.txtBoxNom.value.replace(" ","").replace("-","");
        if (scVal.txtBoxNom.value == "") 
        {
            utilDom.viderElement(scVal.msgMdpB);
            scVal.nomValide = false;
            scVal.msgNom.hidden = true;
        }
        else if (val.length > 2 && val.length < 51) 
        {
            scVal.msgNom.hidden = false;
            scVal.nomValide = true;
            scVal.AfficherMsg(scVal.msgNom,"Nom valide", "green");
        }
        else
        {
            scVal.msgNom.hidden = false;
            scVal.AfficherMsg(scVal.msgNom,"Nom invalide (3 à 50 caractères)", "red");
            scVal.nomValide = false;
        }
         //Event de fin
        if (scVal.nomValide) 
        {
            scVal.eventNom = scVal.txtBoxNom.addEventListener("input", scVal.VerifNom);
        }
        else if (scVal.eventNom != null)
        {
            scVal.eventNom = scVal.txtBoxNom.removeEventListener("input", scVal.VerifNom);
        }
    },

    VerifAdresse :function ()
    {
        scVal.txtBoxAdresse.value = scVal.txtBoxAdresse.value.trim();
        if (scVal.txtBoxAdresse.value == "") 
        {
            utilDom.viderElement(scVal.msgMdpB);
            scVal.adresseValide = false;
        }
        else if (scVal.txtBoxAdresse.value.length > 2 && scVal.txtBoxAdresse.value.length < 201)
        {
            scVal.AfficherMsg(scVal.msgAdresse,"Adresse valide", "green");
            scVal.adresseValide = true;
        }
        else
        {
            scVal.AfficherMsg(scVal.msgAdresse,"Adresse invalide (3 à 200 caractères)", "red");
            scVal.adresseValide = false;
        }
        //Event de fin
        if (scVal.adresseValide) 
        {
            scVal.eventAdresse = scVal.txtBoxAdresse.addEventListener("input", scVal.VerifAdresse);
        }
        else if (scVal.eventNom != null)
        {
            scVal.eventAdresse = scVal.txtBoxAdresse.removeEventListener("input", scVal.VerifAdresse);
        }
    },


    Initialisation : function()
    {
        //form
        scVal.form = document.getElementById("form");
        scVal.form.addEventListener("submit", function(e) {scVal.Authentification(e, false);});
        //nom
        scVal.txtBoxNom = document.getElementById("nom");
        scVal.txtBoxNom.addEventListener("focusout", scVal.VerifNom);
        //scVal.txtBoxNom.required = true;
        //adresse
        scVal.txtBoxAdresse = document.getElementById("adresse");
        scVal.txtBoxAdresse.addEventListener("focusout", scVal.VerifAdresse);
        //scVal.txtBoxAdresse.required = true;
        //mail
        scVal.txtBoxCourriel = document.getElementById("mail");
        scVal.txtBoxCourriel.addEventListener("focusout", scVal.VerifEmail);
        //scVal.txtBoxCourriel.required = true;
        //mot de passe
        scVal.txtBoxMdpA = document.getElementById("mdpA");
        scVal.txtBoxMdpA.addEventListener("focusout", scVal.VerifMdpA);
        //scVal.txtBoxMdpA.required = true;
        scVal.txtBoxMdpB = document.getElementById("mdpB");
        scVal.txtBoxMdpB.addEventListener("focusout", scVal.VerifMdpB);
        //scVal.txtBoxMdpB.hidden = true;

        //messages
        scVal.msgNom = document.getElementById("msgNom");
        scVal.msgMail = document.getElementById("msgMail");
        scVal.msgMdpA = document.getElementById("msgMdpA");
        scVal.msgMdpB = document.getElementById("msgMdpB");
        scVal.msgAdresse = document.getElementById("msgAdresse");
    }
};

scVal.Initialisation();