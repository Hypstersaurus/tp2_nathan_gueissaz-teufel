/**
 * Created by nrichard on 2017-03-16.
 */

"use strict";


var utilAjax = {
    /**
     * Permet d'envoyer une requête AJAX en GET ou POST
     * @param {XMLHttpRequest} clientHttp Votre client HTTP
     * @param {string} url Cible de votre requête
     * @param {string} typeReq Indique est la requête est en GET ou POST
     * @param {Object.<string, string>} parametres Tableau associatif des paramètres de votre requête
     * @param {function} fonctionRetour Fonction à appeler lors de la réception de la réponse
     */
    envoyerRequeteAjax : function(clientHttp, url, typeReq, parametres, fonctionRetour) {
        clientHttp.addEventListener(
            "readystatechange", 
            function() {if ((clientHttp.readyState === 4) && (clientHttp.status !==0)) fonctionRetour();}, 
            false
        );

        var urlCible = url;
        var paramStr = "";
        var indice = 0;

        for (var param in parametres) {
            if (parametres.hasOwnProperty(param)) {
                if (indice > 0) {
                    paramStr += "&";
                }
                paramStr += encodeURIComponent(param) + "=" + encodeURIComponent(parametres[param]);
                indice += 1;
            }
        }

        var body = "";
        if (typeReq.toUpperCase() === "GET") {
            if (Object.keys(parametres).length > 0) {
                urlCible += "?" + paramStr;
            }
        } else {
            body = paramStr;
        }

        clientHttp.open(typeReq, urlCible, true);
        clientHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        clientHttp.send(body);
    }
};