"use strict";


var utilCookies = {
    //retourne la valeur du cookie en paramètre, null si non défini
    GetValeurCookie : function(nomCookie)
    {
        var match = document.cookie.match(new RegExp(nomCookie + '=.*?(?=;|$)'));
        if(match)
        {
            return match[0].substring(nomCookie.length+1);
        }
        else return null;
    },

    SetCookie :  function(nom, valeur)
    {
        var date = new Date();
        date = new Date(date.getTime() + 7 * 24 * 60 * 60 * 1000 * 4);
        document.cookie = nom+"=" + valeur + "; expires="+date.toUTCString()+"; path=/";
    }
};