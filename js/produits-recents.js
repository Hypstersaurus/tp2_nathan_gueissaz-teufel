

"use strict";

//Juste pour dire à JSHINT que les fonctions existent
/*global utilCookies*/
var scRec =
{
    //référence au div affiché actuellement
    "divAffichage"      : null,
    "strListeCookie"    : null,
    "strListeTitres"    : null,
    "msg"               : null,
    
    GetProduits : function()
    {
        return scRec.strListeCookie.split(',');
    },
    
    EstDansListe: function(elem)
    {
        return scRec.strListeCookie.match('(,|^)'+elem+'(&)');
    },
    
    //Prépare l'affichage des récents (mais ne les affiche pas)
    PreparerAffichage: function()
    {
        //titre
        scRec.msg = document.createElement("H2");
        scRec.msg.appendChild(document.createTextNode("Produits récemment visités"));
        scRec.divAffichage.appendChild(scRec.msg);
        if (scRec.strListeCookie == "")
        {
            scRec.msg = document.createElement("H3");
            scRec.msg.appendChild(document.createTextNode("Aucun produit récemment visité"));
            scRec.divAffichage.appendChild(scRec.msg);
        }
        else
        {
            var liste = scRec.GetProduits();
            for (var i = 0; i < liste.length-1; i++) 
            {
                //div
                var contenuRecents = document.createElement("DIV");
                contenuRecents.className = "contenuRecents";
                //a
                var lienProduit = document.createElement("A");
                lienProduit.href = "produit-detail.php/?id="+liste[i].split('&')[0];
                //img
                var imgRec = document.createElement("IMG");
                imgRec.className = "imgContenuRecents";
                imgRec.src = "/images/placeholder.png";
                //h3
                var titreProduit = document.createElement("H4");
                //titrePanier.className = "titrePanier";
                titreProduit.appendChild(document.createTextNode(liste[i].split('&')[1]));
            
                //Mise en place des conteneurs
                scRec.divAffichage.appendChild(contenuRecents);
                contenuRecents.appendChild(lienProduit);
                lienProduit.appendChild(imgRec);
                lienProduit.appendChild(titreProduit);
                scRec.divAffichage.appendChild(document.createElement("BR"));
            }
        }
    },
    
    AffichageDiv: function(e)
    {
        if  ((document.getElementById("imgRecents").contains(e.target)|| 
        scRec.divAffichage.contains(e.target)) && scRec.divAffichage.hidden) scRec.divAffichage.hidden = false;
        else scRec.divAffichage.hidden = true;
    },
    
    //fonction appelée lorsque le <body> d'un produit-détail se fait appelé
    //met à jour le cookie et scRec.strListe
    AjouterProduit: function(id, titre)
    {
        var retour = "";
        //si 
        if (scRec.strListeCookie != "") 
        {
            var liste = scRec.GetProduits();
            retour += id + '&' + titre + ',';
            if (scRec.EstDansListe(id)) 
            {   
                for (var i = 0; liste[i] && i < 5; i++)
                {
                    if (liste[i].split('&')[0] != id) 
                    {
                        retour += liste[i] + ',';
                    }
                }
            }
            else
            {
                for (var j = 0; liste[j] && j < 4; j++)
                {
                    if (liste[j].split('&')[0] != id) 
                    {
                        retour += liste[j] + ',';
                    }
                }
            }
        }
        else
        {
            retour += id +'&'+ titre + ',';
        }
        scRec.strListeCookie = retour;
        utilCookies.SetCookie("recents",retour);
    },
    
    
    initialisation: function() {
    
        //Variables
        scRec.divAffichage = document.getElementById("divAffichageComplet");
        scRec.strListeCookie = utilCookies.GetValeurCookie('recents');
        if (scRec.strListeCookie == null)
        {
            utilCookies.SetCookie("recents", "");
            scRec.strListeCookie = "";
        }
        //Événements clic mouse
        window.addEventListener('click', function(e){scRec.AffichageDiv(e);}, false);
    
        //on prépare l'affichage
        scRec.PreparerAffichage();
    
        //Changement de couleurs sur hover
        document.getElementById("pardessusPanier").addEventListener("mouseover", 
        function() {document.getElementById("logoPanier").style.backgroundColor = "ivory";});
        document.getElementById("pardessusPanier").addEventListener("mouseout", 
        function() {document.getElementById("logoPanier").style.backgroundColor = "transparent";});
    }
};
scRec.initialisation();
//window.addEventListener('load', initialisation, false);