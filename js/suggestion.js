/**
 * Script pour l'affichage de suggestion de recherche
 */

"use strict";

// Pour indiquer à JSHint que ces fonctions sont définies dans d'autres scripts
/*global utilDom*/
/*global scRecherche*/


var scriptSug =
{
    "divSuggestions" : null,

    /**
    * Appelée lors d'un clic dans la fenêtre
    */
    gererClicFenetre : function(evenement) {

        //cliqué sur la liste?
        var clicDedans = scriptSug.divSuggestions.contains(evenement.target) ||
        //cliqué sur le textbox?
        scRecherche.txtBoxRecherche.contains(evenement.target);

        //si le textbox est cliqué on fait la recherche (on vérifie si elle n'a pas déjà été faite pour ne pas réenvoyer de get)
        if (clicDedans && scriptSug.divSuggestions.hidden && scRecherche.txtBoxRecherche.value.length > 2) 
        {
            scriptSug.divSuggestions.hidden = false;
        }

        if (!clicDedans) {
            scriptSug.divSuggestions.hidden = true;
        }
    },

    //Gère le eventHandler de chacun des 
    suggSelectHandler : function(id, balise)
    {
        balise.addEventListener("click", function() 
        {
            scRecherche.txtBoxRecherche.value = balise.innerText;
            utilDom.viderElement(scriptSug.divSuggestions);
            scriptSug.divSuggestions.hidden = true;
            scRecherche.txtBoxRecherche.focus();
            window.open("produit-detail.php/?id="+id);
        });
    },


    /**
     * Pour demander les suggestions au site web.
     */
    afficherSuggestions : function () 
    {
        utilDom.viderElement(scriptSug.divSuggestions);
        scriptSug.divSuggestions.hidden = true;
        var reponse = null;
        try {
            reponse = JSON.parse(scRecherche.clientHttpSugg.responseText);
        } catch (e) {
            window.console.error(
                "La réponse AJAX n\'est pas une expression JSON valide.");
            window.console.error(e.message);
            window.console.error(scRecherche.clientHttp.responseText);
        }
        if (reponse[0].trouve) 
        {
            scriptSug.divSuggestions.hidden = false;
            var elemListe = document.createElement('ul');
            elemListe.className = "ulSuggestions";
            scriptSug.divSuggestions.appendChild(elemListe);

            var itemListe = [];
            for (var i=1;i<reponse.length;i++) 
            {
                itemListe[i-1] = document.createElement('li');
                itemListe[i-1].className = "liSuggestions";
                itemListe[i-1].appendChild(document.createTextNode(reponse[i].titre));
                elemListe.appendChild(itemListe[i-1]);
                scriptSug.suggSelectHandler(reponse[i].idProduit, itemListe[i-1]);
            }
        }
     },


    /**
     * Appelée lors de l'initialisation de la page
     */
    initialisation : function()
    {
        scriptSug.divSuggestions = document.getElementById("divSuggestions");
        // Ajout d'un événement sur tout le document (la fenêtre)
        document.addEventListener('click', scriptSug.gererClicFenetre, false);
    }
};

window.addEventListener('load', scriptSug.initialisation, false);
