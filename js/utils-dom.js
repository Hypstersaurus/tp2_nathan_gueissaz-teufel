
"use strict";
var utilDom = {
  /**
   * Pour vider un élément DOM.
   *
   * @param element L'élément à vider
   */
  viderElement : function (ele){
    while (ele.lastChild) {
      ele.removeChild(ele.lastChild);
    }
  }
};