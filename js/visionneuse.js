
"use strict";

/*global utilCookies*/
/*global utilAjax*/
/*global utilDom*/


var scVisio =
{
    "divPop"      : null, 
    "lesProduitsPop": null,
    "clientHttp" : null,
    "minuterie" : null,
    "divGeneral" : null,
    "noAffiche" : null,


    PanelSuivPre : function (i)
    {
        document.getElementById("produit"+scVisio.noAffiche).style.display = "none";
        scVisio.noAffiche += i;
        if (scVisio.noAffiche==5) 
        {
             scVisio.noAffiche = 0;
        }
        else if (scVisio.noAffiche == -1)
        {
            scVisio.noAffiche = 4;
        }
        document.getElementById("produit"+scVisio.noAffiche).style.display = "inline-block";
    },

    InitialiserIntervale : function()
    {
        scVisio.minuterie = window.setInterval(function() {scVisio.PanelSuivPre(1);}, 5000);
    },

    GererMouseOver : function(sender)
    {
        if (scVisio.minuterie != null) 
        {
            clearInterval(scVisio.minuterie);
            scVisio.minuterie = null;
        }
    
    },

    GererMouseOut: function(sender)
    {
        if (scVisio.minuterie == null) scVisio.InitialiserIntervale();
    },

    AjoutBoutons : function()
    {
        var div = document.createElement("DIV");
        scVisio.divPop.appendChild(div);
        var bouton = document.createElement("BUTTON");
        bouton.appendChild(document.createTextNode("<<"));
        div.appendChild(bouton);
        bouton.addEventListener("click", function() {scVisio.PanelSuivPre(1);}, false);
        bouton.style.marginBottom = "20px";
        bouton.style.marginRight = "5px";

        var boutonpre = document.createElement("BUTTON");
        boutonpre.appendChild(document.createTextNode(">>"));
        div.appendChild(boutonpre);
        boutonpre.addEventListener("click", function() {scVisio.PanelSuivPre(-1);}, false);
        boutonpre.style.marginBottom = "20px";
        bouton.style.marginLeft = "5px";
    },

    //Crée et ajoute les nodes 
    PreparerAffichagePanels : function()
    {
        var titre = document.createElement("H2");
        scVisio.divPop.appendChild(titre);
        titre.appendChild(document.createTextNode("Top 5 produits les plus appréciés"))
        for (var i = 0; i < 5; i++) 
        {
            //div
            var contenuIndex = document.createElement("DIV");
            contenuIndex.className = "contenuProduitIndex";
            scVisio.divPop.appendChild(contenuIndex);
            contenuIndex.id = "produit"+i;
            contenuIndex.style.display = "none";
            //a
            var lienProduit = document.createElement("A");
            lienProduit.className = "lienImage";
            lienProduit.href = "/sources/produit-detail.php/?id="+scVisio.lesProduitsPop[i].idProduit;
            contenuIndex.appendChild(lienProduit);
            //img
            var imgRec = document.createElement("IMG");
            imgRec.className = "imgProduitIndex";
            imgRec.src = "/images/placeholder.png";
            lienProduit.appendChild(imgRec);
            //ol
            var olInfo = document.createElement("OL");
            olInfo.className = "contenuIndex";
            lienProduit.appendChild(olInfo);
            //li
            var liInfo1 = document.createElement("LI");
            liInfo1.appendChild(document.createTextNode("ID: " + scVisio.lesProduitsPop[i].idProduit));
            olInfo.appendChild(liInfo1);
            var liInfo2 = document.createElement("LI");
            liInfo2.appendChild(document.createTextNode("Titre: " + scVisio.lesProduitsPop[i].titre));
            olInfo.appendChild(liInfo2);
            var liInfo3 = document.createElement("LI");
            liInfo3.appendChild(document.createTextNode("Quantité: " + scVisio.lesProduitsPop[i].quantite));
            olInfo.appendChild(liInfo3);
        }
        scVisio.noAffiche = 0;
        document.getElementById("produit0").style.display = "inline-block";
        scVisio.AjoutBoutons();
    },


    //Gère le retour du JSON
    InitialiserAjaxOut : function()
    {
        if (scVisio.clientHttp.status === 200) {
            try {
                 var reponse = JSON.parse(scVisio.clientHttp.responseText);
                 scVisio.lesProduitsPop = reponse[0];

             } catch (e) {
                 window.console.error(
                     "La réponse AJAX n\'est pas une expression JSON valide.");
                 window.console.error(e.message);
                 window.console.error(scVisio.clientHttp.responseText);
             } 
             scVisio.PreparerAffichagePanels();
             scVisio.InitialiserIntervale();
         } else {
             window.console.error(
                scVisio.clientHttp.status + 
                 " : " + scVisio.clientHttp.responseText);
         }
         scVisio.clientHttp = null;
    },

    //Initialise et attribue la variable lesProduitsPop
    InitialiserAjax : function()
    {
        scVisio.clientHttp = new XMLHttpRequest();
        var parametres = {
            "Intialisation" : true
        };
        utilAjax.envoyerRequeteAjax(
            scVisio.clientHttp,
            "/ajax/visionneuse.php",
            "GET",
            parametres,
            scVisio.InitialiserAjaxOut
        );
    },


    /**
     * Appelée lors de l'initialisation de la page
     */
    initialisation : function() {
        //Variables
        scVisio.divPop = document.getElementById("divPop");
        scVisio.divPop.style.display = "flex";
        document.getElementById("divQte").style.display = "none";
        scVisio.divGeneral = document.getElementById("contenantGeneral");
        scVisio.InitialiserAjax();

        //Événements mouseover et out
        scVisio.divPop.addEventListener('mouseover', function() {scVisio.GererMouseOver(scVisio.divPop);}, false);
        scVisio.divPop.addEventListener('mouseout', function() {scVisio.GererMouseOut(scVisio.divPop);}, false);

        utilDom.viderElement(scVisio.divPop);
    }
};

window.addEventListener('load', scVisio.initialisation, false);
