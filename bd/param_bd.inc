<?php
    // Paramètres de connexion à la BD
    $bdHote = "localhost";
    // Nom d'utilisateur
    $bdUtilisateur = "garneau";
    // Mot de passe
    $bdMotPasse = "qwerty123";
    // Nom de la bd
    $bdNom = "tp1_magasin";
    
    //Définition pour le code dessous
    define("BD_HOTE", $bdHote);
    define("BD_NOM", $bdNom);
    define("BD_UTIL", $bdUtilisateur);
    define("BD_MDP", $bdMotPasse);

    function creerConnexion()
    {
    $connBD = new PDO(
        "mysql:host=" . BD_HOTE . "; dbname=" . BD_NOM,
        BD_UTIL,
        BD_MDP,
        array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) // Pour lancer les exceptions lorsqu'il y des erreurs PDO.
    );
    return $connBD;
}
?>