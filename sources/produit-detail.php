<?php 
    require_once $_SERVER['DOCUMENT_ROOT'] . "/include/classes/produits.php";
    require_once $_SERVER['DOCUMENT_ROOT'] . "/include/cookies.php";
    //creation objet de gestion de produits
    $produitDAO = new ProduitsDAO(CreerConnexion());
    
    //Dirige vers page erreur 404 si produit non trouvé
    try 
    {
        //cherche ID
        $idProd = $_GET['id'];
        $leProduit = $produitDAO->getUnProduit($idProd);
        if (empty($leProduit)) throw new Exception();
        //le form a été appuyé
        if (isset($_GET['x'])) 
        {
            //ajout au panier
            $produitDAO->ajouterQuantiteProduitPanier($leProduit["idProduit"], 1, $leProduit["quantite"]);
            //reload la page sans les x= et y=
            $url = $_SERVER['REQUEST_URI'];
            $index = strpos($url, '&x') - strlen($url);
            $url = substr($url, 0, $index);
            header('Refresh:0; url='.$url);
        }
    } catch (Throwable $th) {
        echo "<h1>404</h1> La page que vous essayez de rejoindre n'existe pas: <br> Numéro de produit invalide";
        exit;
    }
    //Si il n'y pas eu de catch, on continue la page normalement
    require($_SERVER['DOCUMENT_ROOT'] ."/include/header.php");
?>
<body onload="scRec.AjouterProduit(<?php echo $leProduit['idProduit']. ', \'' . $leProduit['titre'] . '\'';?>)">
    <div class=produitDet>
    <?php
        //Affichage de l'image du produit
        $imgSrc= '/images/produits/'.$_GET['id'].'.jpg';
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $imgSrc)) 
        {
            $imgSrc="/images/placeholder.png";
        }
        //calcul note
        if(($leProduit["nbVotes"] > 0))
        {
            $note = round($leProduit["somme"]/$leProduit["nbVotes"], 2) . "/5";
        }
        else
        {
            $note = "Aucune";
        }

        echo '<img src="'.$imgSrc.'" class="imgDetailProduit">';
        echo '<h1 class="titreProduitDetail">'.$leProduit["titre"].'</h1>';
        //echo  '<img src="/images/ajouter-panier.png" title="Ajouter au panier" class="imgBouttonAjouterPanier" id="imgAjoutPanier" hidden
        //onclick="scPanier.AjouterQteProduit('.$leProduit["idProduit"].',1)">';
        ?>
        <!--script type="text/javascript">document.getElementById("imgAjoutPanier").hidden = false;</script-->
        <?php if (intval($leProduit["quantite"]) > 0) {
            $produitDAO->afficherBouttonAjouterAuPanier($leProduit['idProduit']);
        }
        ?>
        <?php
        echo '<h2 class="prix">'.$leProduit["prix"].' $</h2><br>';
        echo '<h3 class="titreProduitDetail">Note: '.$note.'</h3>';
        echo 'En stock: ' . $leProduit["quantite"] . '<br>ID: '.$leProduit["idProduit"].'<br>';
        echo $leProduit["description"].'</h1>';
    ?>
    </div>
<?php require_once('../include/footer.html'); ?>
</body>
</html>