<?php 
    require('../include/header.php');
    require_once $_SERVER['DOCUMENT_ROOT'] . "/include/cookies.php";

    //no script
    $key = '';
    if(isset($_GET['key']) && $_GET['key'] != '')
    {
        $key = $_GET['key'];
        $produitDAO = new ProduitsDAO(CreerConnexion());
        $listeRecherche = $produitDAO->RechercheProduit($key, $_GET['off'], $_GET['orderby']);
    }
?>
<script defer="defer" type="text/javascript" src="../js/suggestion.js"></script>
<script defer="defer" type="text/javascript" src="../js/recherche.js"></script>
<body>
    <!--Sans script: form renvoie vers cette page-->
    <noscript><form action="recherche.php" class="recherche"></noscript>
    <!--Avec script: javascript s'occupe du form-->
    <form onsubmit="return false" class="recherche">
    <h1>Recherche de produit</h1>
        <?php
            echo '<input type="text" value= "'.$key. '" name="key" id="txtRecherche" autocomplete="off">';
            echo '<div id="divSuggestions" hidden></div>';
            echo '<input type="hidden" value= "0" name="off">';
            
            //RADIOBUTTONS orderBy
            echo '<h2>Trier par:</h2>';
            echo '<input type="radio" name="orderby" value="aucun" id="aucun" checked="checked"> ' .
            '<label for="aucun">Aucun</label><br>';
            echo '<input type="radio" name="orderby" value="desc" id="asc">' .
            '<label for="asc">Prix Max</label><br>';
            echo '<input type="radio" name="orderby" value="asc" id="desc">' .
            '<label for="desc">Prix Min</label><br><br>';
        ?>
        <input type="submit" value="Rechercher">
    </form>

    <!--RESULTATS RECHERCHE-->
    <noscript>
    <div id="contenantDeContenantRech" class="contenantDeContenantRech">
    <?php
        if (isset($listeRecherche)) 
        {
           
            //aucun élément trouvé
            if ($listeRecherche === FALSE) 
            {
                echo "<br><br><br><br><br><h3>Votre recherche n'a pas retourné de résultats</h3>";
            }
            else
            {
                for ($i=0; $i < count($listeRecherche) ; $i++) 
                { 
                    echo '<div class="contenuRecherche"><a href="produit-detail.php/?id='.$listeRecherche[$i]["idProduit"].
                    '"><img src="/images/placeholder.png" class="imgRech">';
                    //titre
                    echo '<h3 class="titrePanier">'.$listeRecherche[$i]["titre"].'</h3><br></a>';
                    //prix
                    echo 'Prix = ' . $listeRecherche[$i]["prix"] . '$<br><br>';
                    //description
                    echo $listeRecherche[$i]["description"];
                    echo '</div>';
                }

            }
            //PAGINATION RECHERCHE
            echo '<br><br><br><div class="paginationRecherche"';
            $disabled = '';

             //précédent
            $disabled = '';
            if ($_GET['off'] == 0) 
            {
                $disabled = ' disabled';
            }

            $avant = $prochain = $_GET['off']-1;
            echo '<form action="recherche.php"> '.
            '<input type="hidden" name="key" value="'.$_GET['key'].'">'.
            '<input type="hidden" name="off" value="'.$avant.'">'.
            '<input type="hidden" name="orderby"value="'.$_GET['orderby'].'">'.
            '<button type="submit"'.$disabled.'>10 Retour</button>'.
            '</form>';

            $disabled = '';
            //suivant
            if ($produitDAO->RechercheProduit($key, $_GET['off']+1, $_GET['orderby']) === FALSE) 
            {
                $disabled = ' disabled';
            }

            $prochain = $_GET['off']+1;
            echo '<form action="recherche.php"> '.
            '<input type="hidden" name="key" value="'.$_GET['key'].'">'.
            '<input type="hidden" name="off" value="'.$prochain.'">'.
            '<input type="hidden" name="orderby" ' . $disabled . ' value="'.$_GET['orderby'].'">'.
            '<button type="submit"'.$disabled.'>10 Prochains</button>'.
            '</form></div>';
        }
    ?>
    </div>
    </noscript>

    <div id="contenantDeContenantRech" class="contenantDeContenantRech" hidden>
    </div>
    <br>
</body>

<?php require_once('../include/footer.html'); ?>
