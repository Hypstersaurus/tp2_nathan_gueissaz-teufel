<?php

session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //validation du nom avec regex
    if(preg_match("/^[a-zA-Z' -]+$/", $_POST['nom'])){
        if(filter_var($_POST['courriel'], FILTER_VALIDATE_EMAIL)){
            if($_POST['mdp1'] == $_POST['mdp2']){
                require '../include/classes/utilisateurs-dao.php';
                require '../bd/param_bd.inc';
                $connexionBD = creerConnexion();

                $utilDAO = new UtilisateurDAO($connexionBD);
                $_POST['mdp'] = $_POST['mdp1'];

                $utilDAO->ajoutUtilisateurBD(
                    $_POST['nom'],
                    $_POST['courriel'],
                    $_POST['mdp'],
                    $_POST['adresse']
                );
                echo '<h1>Compte créé avec succès</h1> <a href="../index.php">Retour à l\'accueil</a>';
                exit;

                /*if($utilDAO->validerUtilisateurDejaExistant($utilisateur)){
                    //error l'email existe déjà
                }else{
                    
                    $utilDAO->ajoutUtilisateurBD($utilisateur);
                    
            
                    $temp = $utilisateur;
                    unset($temp["IdUtilisateur"]);
                    $req->execute($temp);
                    $utilisateur['IdUtilisateur'] = ($this->connBd->lastInsertId());
            
                    $req->closeCursor();

                    if ($utilDAO->verificationUtilisateur($utilisateur)) {
                        $_SESSION['IdUtilisateur'] = $utilisateur['IdUtilisateur'];
                        $_SESSION['Nom'] = $utilisateur['Nom'];
                        $_SESSION['EstAdmin'] = $utilisateur['EstAdmin'];

                        //redirection une fois le processus terminé
                        //TODO: Redirection vers une autre page plus générique?
                        header('location:./recherche.php');
                    }
                }*/
            }
        }
    }
}

require('../include/header.php');
?>
<script defer="defer" type="text/javascript" src="/js/validation.js"></script>
<body>
    
    <?php
    //require_once $_SERVER['DOCUMENT_ROOT'] . '/include/header.html';
    ?>
    
    <main id="main">
        <form action="creationcompte.php" id="form" method="post">
            <h1>Créer un compte</h1>
            <p class="inputCompte">
                <label for="nom">Nom:</label>
                <br />
                <input type="text" name="nom" id="nom" />
            </p>
            <p id="msgNom" class= "msgCompte" hidden></p>
            <p class="inputCompte">
                <label for="courriel">Courriel:</label>
                <br />
                <input type="text" name="courriel" id="mail" placeholder="example@example.com" /><br />
            </p style="margin:0">
            <p id="msgMail" class= "msgCompte" hidden></p>
            <p class="inputCompte">
            <label for="adresse">Adresse:</label><br>
            <input type="text" name="adresse" id="adresse" placeholder="123 rue abc, ville, province"/>
            </p>
            <p id="msgAdresse" class="msgCompte"></p>
            <p class="inputCompte">
                <label for="mdp">Mot de passe:</label>
                <br />
                <input type="password" name="mdp1" id="mdpA" placeholder="Inscrire votre mot de passe ici" style="margin-bottom: 5px"/>
                <br />
                <input type="password" name="mdp2" id="mdpB" placeholder="Retapez votre mot de passe une deuxième fois" />
                <br>
            </p>
            <div id="msgMdpDiv" style="margin-top:-5px; margin-bottom:5px;">
            <p id="msgMdpA" class= "msgCompte" hidden></p><br>
            <p id="msgMdpB" class= "msgCompte" hidden></p><br>
            </div>
            <input type="submit" name="submit" value="Créez le compte">
            <input type="reset" name="reset" value="Réinitialiser">
        </form>
    </main>
    <p>Déjà un compte? <a href = "/sources/authentification.php">S'authentifier</a></p>
    <?php
    //require_once $_SERVER['DOCUMENT_ROOT'] . '/include/footer.html';
    ?>
</body>
<?php

require_once('../include/footer.html');
?>

</html>