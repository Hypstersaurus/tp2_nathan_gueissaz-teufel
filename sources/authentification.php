<?php
session_start();

if (!empty($_POST["souvenir"])) {
    setcookie("courriel", $_POST["courriel"], time() + (10 * 365 * 24 * 60 * 60));
} else {
    if (isset($_COOKIE["courriel"])) {
        setcookie("courriel", "");
    }
}

$nom = $courriel = $mdp = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST['courriel'])) {
        if (!empty($_POST['mdp'])) {

            require '../bd/param_bd.inc';
            require '../include/classes/utilisateurs-dao.php';
            
            $connexionBD = creerConnexion();

            $userDAO = new UtilisateurDAO($connexionBD);

            $utilisateur = $userDAO->creerUtilisateur(
                $_POST['courriel'],
                $_POST['mdp']
            );

            if ($userDAO->verificationUtilisateur($utilisateur)) {
                $_SESSION['IDUtil'] = $utilisateur['IDUtil'];
                $_SESSION['nom'] = $utilisateur['nom'];
                $_SESSION['EstAdmin'] = $utilisateur['EstAdmin'];

                if(isset($_GET['panier']))
                {
                    header('location:mon-panier.php');
                }
                else header('location:../index.php');
            }
        }
    }
    $courriel = $_POST['courriel'];
    $mdp = $_POST['mdp'];
}
?>

<!DOCTYPE html>
<?php
require('../include/header.php');
?>
<body>

    <main>
        <form action="#" method="post">
            <h1>Connexion</h1>
            <p id>
                <label for="courriel">Courriel:</label>
                <input type="text" name="courriel" value="<?php if (isset($_COOKIE['courriel'])) {
                                                            echo $_COOKIE['courriel'];
                                                        } ?>" />
            </p>
            <p>
                <label for="mdp">Mot de passe:</label>
                <input type="password" name="mdp" />
            </p>
            <p>
            Pas de compte? <a href= "/sources/creationcompte.php">Créer un compte</a>
            </p>
            <p>
                <label for="souvenir">Se souvenir de moi</label>
                <input type="checkbox" name="souvenir" <?php if (isset($_COOKIE["courriel"])) { ?> checked <?php } ?> />
            </p>
            <input type="submit" name="submit" value="Connexion">
            <input type="reset" name="reset" value="Réinitialiser">
        </form>
    </main>
    <?php
        require_once('../include/footer.html');
    ?>
</body>
</html>