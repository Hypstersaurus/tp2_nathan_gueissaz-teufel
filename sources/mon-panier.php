<?php 
    //cookies
    require_once $_SERVER['DOCUMENT_ROOT'] . "/include/cookies.php";
    //creation objet de gestion de produits
    require_once $_SERVER['DOCUMENT_ROOT'] . "/include/classes/produits.php";
    $produitDAO = new ProduitsDAO(CreerConnexion());

    //si la suppression/modif d'un élément du panier est effectuée
    if (isset($_GET['id']))
    {
        if(strstr($_COOKIE['panier'],$_GET['id']) === FALSE)
        {
            header("Location: /404.html");
            die();
        }
        //modif qt
        if(isset($_GET['qt']))
        {
            $produitDAO->ajouterQuantiteProduitPanier($_GET['id'], $_GET['qt'], $_GET['stock']);
            $url = $_SERVER['REQUEST_URI'];
            $index = strpos($url, '?') - strlen($url);
            $url = substr($url, 0, $index);
            header('Refresh:0; url='.$url);
        }
        //suppression
        else
        {
            $produitDAO->RetirerDuPanier($_GET['id']);
            $url = $_SERVER['REQUEST_URI'];
            $index = strpos($url, '?') - strlen($url);
            $url = substr($url, 0, $index);
            header('Refresh:0; url='.$url);
        }
    }
    else if (isset($_GET['caisse'])) 
    {
        $produitDAO->Caisse();
    }

    //Inclusion header
    echo '<body>';
    require  $_SERVER['DOCUMENT_ROOT'] . '/include/header.php';
?>

    <h1 class="titreIndex">Mon panier</h1><br>
    <div class="contenantDeContenantPanier">
        <?php
            $strListe = $_COOKIE['panier'];
            //Y a t'il des éléments dans le panier/le cookie existe-t-il
            if (isset($_COOKIE['panier']) && $strListe != '') 
            {
                if (isset($_SESSION['IDUtil'])) 
                {
                    $lienCaisse = '/sources/mon-panier.php';
                }
                else $lienCaisse = '/sources/creationcompte.php/?panier=true';
                echo '<form class="formFinPanier" method="get" action="'. $lienCaisse .'">'.
                '<button class="btnFinPanier"  type=submit value="oui" name="caisse">Passer à la caisse</button></form>';
                $liste = $produitDAO->GetElemsPanier($strListe);

                for ($i=0; $i < count($liste)-1; $i++)
                {
                    $quantite = $liste[$i][1];
                    echo '<div class="contenantPanier"><a href="produit-detail.php/?id='.$liste[$i][0]["idProduit"].
                    '"><img src="/images/placeholder.png" class="imgPanier"></a>';
                    //bouton X
                    echo '<form method="get" action="/sources/mon-panier.php">'.
                    '<input type=hidden value="'.$liste[$i][0]["idProduit"].'" name="id">'.
                    '<input type="submit" value="X" class="boutonPanierX"></form>';
                    //titre
                    echo '<h2 class="titrePanier">'.$liste[$i][0]["titre"].'</h2><br>';
                    //=====forms quantité=====
                    echo '<h3 class="formQt">Quantité:'. $liste[$i][1];
                    //bouton -
                    echo ' <form class="formQt" method="get" action="/sources/mon-panier.php">'. 
                    '<input type=hidden value="'.$liste[$i][0]["idProduit"].'" name="id">'.
                    '<input type=hidden value="'.$liste[$i][0]["quantite"].'" name="stock">'.
                    '<button class="btnQtPanier" type=submit value="-1" name="qt">-</button></form> ';
                    //bouton +
                    echo'<form class="formQt" method="get"action="/sources/mon-panier.php">'.
                    '<input type=hidden value="'.$liste[$i][0]["idProduit"].'" name="id">'.
                    '<input type=hidden value="'.$liste[$i][0]["quantite"].'" name="stock">'.
                    '<button class="btnQtPanier" type=submit value="1"'.
                    '" name="qt">+</button></form></h3><br>';
                    //=========================
                    //description
                    echo $liste[$i][0]["description"];
                    echo '</div>';
                }
            }
            else
            {
                echo "Pas de produits dans le panier! Ajoutez en en cliquant sur l'icone de panier sur un qu'il nous reste en stock!";
            }
        ?>
    </div>
    <?php

         require_once('../include/footer.html');
    ?>
</body>
</html>

