<?php
    session_start();
    session_destroy();
?>

<body>
    <?php 
        require('../include/header.php');
    ?>
    <main>
        <h1>Vous êtes désomais déconnecté</h1>
        <p><a href="../index.php">Retour à la page d'acceuil</a></p>
    </main>
</body>
