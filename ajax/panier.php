<?php
    //Nathan Gueissaz-Teufel
    //DA: 1834170
    require_once "utils-ajax.php";
    require_once "../include/classes/produits.php";

    ecrireEnteteJson();
    $reponse = array();
    $strPanier = $_COOKIE['panier'];
    if (!isset($strPanier) || explode('.',$strPanier)[0] = "")
    {
        $reponse = null;
    } 
    else 
    {
        $produitDAO = new ProduitsDAO(CreerConnexion());
        $strPanier = explode('.',$strPanier);
        for ($i=0; $i < count($strPanier)-1; $i++) 
        { 
            $strPanier[$i] = explode('=',$strPanier[$i]);
            $strPanier[$i] = $produitDAO->GetUnProduit($strPanier[$i][0]);
            if ($strPanier[$i] != null) 
            {
                array_push($reponse, $strPanier[$i]);
            }
        }
    }

    echo json_encode($reponse, JSON_PRETTY_PRINT);
?>