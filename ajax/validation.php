<?php

require_once("..\include\classes\utilisateurs-dao.php");
require_once('../bd/param_bd.inc');
require_once('..\ajax\utils-ajax.php');

$connexionBD = creerConnexion();

$utilisateurDao = new UtilisateurDAO($connexionBD);

ecrireEnteteJson();

if(isset($_GET["mail"]))
{
    $errEmail = "L'adresse courriel est valide";
    $email = $_GET["mail"];
    //$email = ParseText($_GET["email"]);
    if(empty($email))
    {
        $errEmail = "Valeur nulle entrée";
    }
    else if(strlen($email) > 50)
    {
        $errEmail = "Courriel entré trop long";
    }
    else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $errEmail = "Courriel invalide";
    }
    else if($utilisateurDao->UtilisateurExiste($email))
    {
        $errEmail = "Un compte est déjà associé à ce courriel";
    }
    echo json_encode($errEmail, JSON_PRETTY_PRINT);
}
else if (isset($_GET["creer"]) && isset($_GET["courriel"]) && isset($_GET["mdp"]) && isset($_GET["adresse"]) && isset($_GET["nom"]))
{
    $creer = $_GET["creer"];
    $courriel = $_GET["courriel"];
    $mdp = $_GET["mdp"];
    $adresse = $_GET["adresse"];
    $nom = $_GET["nom"];
    $utilisateurDao->ajoutUtilisateurBD($utilisateurDao->creerUtilisateur($courriel, $mdp, $nom, $adresse));
}

?>