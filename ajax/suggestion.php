<?php
    //Nathan Gueissaz-Teufel
    //DA: 1834170
    require_once "utils-ajax.php";
    require_once "../include/classes/produits.php";

    ecrireEnteteJson();

    $reponse = array();
    $resultatSelect;

    $strRecherche = $_GET["suggestion"];
    if (!isset($strRecherche)) {
        $reponse = declarerErreur("Paramètre recherche manquant", 400);
    } else {
        $produitDAO = new ProduitsDAO(CreerConnexion());
        
        //=====================SELECT=======================
        $req = $produitDAO->connBd->prepare('SELECT idProduit, titre FROM produit WHERE titre REGEXP \'^'. $strRecherche .'\'' .
        ' ORDER BY LENGTH(titre) ASC LIMIT 5');
        $req->execute();
        $resultatSelect = $req->fetchAll();
        $req->closeCursor();

        if (empty($resultatSelect))
        {
            //0 résultats
            array_push($reponse, array("trouve" => false));
        }
        else
        {
            array_push($reponse, array("trouve" => true));
            for ($i=0; $i < count($resultatSelect); $i++) 
            { 
                array_push($reponse, $resultatSelect[$i]);
            }
            
        }
        
    }

    echo json_encode($reponse, JSON_PRETTY_PRINT);
?>