<?php
    //Nathan Gueissaz-Teufel
    //DA: 1834170
    require_once "utils-ajax.php";
    require_once "../include/classes/produits.php";

    ecrireEnteteJson();

    $reponse = array();

    $strRecherche = $_GET["recherche"];
    $strOffset = $_GET["offset"];
    $strOrder = $_GET["order"];
    if (!isset($strRecherche) || !isset($strOffset) || !isset($strOrder)) {
        $reponse = declarerErreur("Paramètre recherche manquant", 400);
    } else {
        $produitDAO = new ProduitsDAO(CreerConnexion());

        $resultatRecherche = $produitDAO->RechercheProduit($strRecherche, $strOffset, $strOrder);
        if ($resultatRecherche === false)
        {
            //0 résultats
            array_push($reponse, array("trouve" => false));
        }
        else
        {
            //+ résultats
            array_push($reponse, array("trouve" => true));
            for ($i=0; $i < count($resultatRecherche) ; $i++) 
            { 
                $p = array(
                    "id" => $resultatRecherche[$i]["idProduit"],
                    "titre"   => $resultatRecherche[$i]["titre"],
                    "prix"      => $resultatRecherche[$i]["prix"],
                    "description" => $resultatRecherche[$i]["description"]
                );
                array_push($reponse, $p);
            }
        }
        
    }

    echo json_encode($reponse, JSON_PRETTY_PRINT);
?>