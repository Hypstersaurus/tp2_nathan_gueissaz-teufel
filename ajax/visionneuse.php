<?php
    //Nathan Gueissaz-Teufel
    //DA: 1834170
    //Renvoie 2 tableaux
    require_once "utils-ajax.php";
    require_once "../include/classes/produits.php";

    ecrireEnteteJson();

    $reponse = array();
    $produitDAO = new ProduitsDAO(CreerConnexion());

    array_push($reponse, $produitDAO->listerCinqPlusNote());
    
    echo json_encode($reponse, JSON_PRETTY_PRINT);
?>