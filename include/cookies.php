<?php
    //cookies
    //cookie stockant les produits mis dans le panier
    //ex:   "12,34,"= 2 produits
    //      "" =      0 produit
    //      "234,"=   1 produit
    if(!isset($_COOKIE['panier'])) 
    {
        setrawcookie('panier', '', time()+60*60*24, '/');
        //set localement le cookie (pas avoir à attendre une réponse du poste client pour utiliser le cookie)
        $_COOKIE['panier'] = '';
    }
    // //le form a été appuyé sur produit-detail.php
    // if (isset($_GET['id']) && isset($_GET['x'])) 
    // {
    //     require_once $_SERVER['DOCUMENT_ROOT'] . "/include/classes/produits.php";
    //     $idProd = $_GET['id'];
    //     $produitDAO = new ProduitsDAO(CreerConnexion());
    //     $leProduit = $produitDAO->getUnProduit($idProd);
    //     //ajout au panier
    //     $produitDAO->ajouterQuantiteProduitPanier($leProduit["idProduit"], 1, $leProduit["quantite"]);
    //     //reload la page sans les x= et y=
    //     $url = $_SERVER['REQUEST_URI'];
    //     $index = strpos($url, '&x') - strlen($url);
    //     $url = substr($url, 0, $index);
    //     header('Refresh:0; url='.$url);
    // }
?>