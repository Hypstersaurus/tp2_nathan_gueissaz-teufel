<?php 

    require_once $_SERVER['DOCUMENT_ROOT'] . "/bd/param_bd.inc";

class ProduitsDAO
{
    public $connBd;

    /**
     * Constructeur
     *
     * @param PDO $connBd Référence vers la BD
     */
    public function __construct(PDO $connBd)
    {
        $this->connBd = $connBd;
    }


    /**
     * Pour créer un Jeu Vidéo
     */
    public static function CreerProduit(
        $idProduit,
        $titre,
        $description,
        $prix,
        $quantite,
        $somme,
        $nbVotes
    ) {
        // En utilisant le nom des champs dans la BD
        return array(
            'idProduit'     => $idProduit,
            'titre'         => $titre,
            'description'   => $description,
            'prix'          => $prix,
            'quantite'      => $quantite,
            'somme'         => $somme,
            'nbVotes'       => $nbVotes
        );
    }

    /**
     * Pour ajouter une jeu
     * @param $jeu Le jeu passé par référence
     * @throws Exception
     */
    public function ajouter(&$produit)
    {

        var_dump($jeu);

        $req = $this->connBd->prepare('INSERT INTO produit(titre,description,prix,quantite,somme,nbVotes) ' .
                ' VALUES(:titre,:description,:prix,:quantite,:somme,:nbVotes)'
        );
        
        // Bonne façon ;)
        $req->execute([
            'titre' => $produit['titre'],
            'description' => $produit['description'],
            'prix' => $produit['prix'],
            'quantite' => $produit['quantite'],
            'somme' => $produit['somme'],
            'nbVotes' => $produit['nbVotes'],
            ]);

        /*
            Malheureusement, notre $jeu a la clé "id" qui n'existe pas dans 
            la requête. Nous devons faire une copie conforme et enlever la
            clé "id".
        */
        $param = $produit;          // Copie conforme
        unset($produit["idProduit"]);    // Enlever la clé "id"
        $req->execute($param);

        $produit['idProduit'] = ($this->connBd->lastInsertId());


        var_dump($produit);

        $req->closeCursor();
    }

    /**
     * Retourne tous les produits
     * @return array Liste des produits
     * @throws Exception Si un problème SQL
     */
    public function listerTous()
    {
        $req = $this->connBd->prepare('SELECT idProduit,titre,description,prix,quantite,somme,nbVotes FROM produits');
        $req->execute();

        $produits = $req->fetchAll();
        $req->closeCursor();
        return $produit;
    }

    public function listerCinqPlusQuantite()
    {
        $req = $this->connBd->prepare(
            'SELECT idProduit,titre,description,prix,quantite,somme,nbVotes ' .
            'FROM produit ' .
            'ORDER BY quantite ' .
            'DESC LIMIT 5');
        $req->execute();

        $produits = $req->fetchAll();
        $req->closeCursor();
        return $produits;
    }

    
    public function listerCinqPlusNote()
    {
        $req = $this->connBd->prepare(
            'SELECT idProduit,titre,description,prix,quantite,somme,nbVotes ' .
            'FROM produit ' .
            'ORDER BY nbVotes ' .
            'DESC LIMIT 5');
        $req->execute();

        $produits = $req->fetchAll();
        $req->closeCursor();
        return $produits;
    }

    //retourne un array avec les données du produit trouvé avec l'ID fourni
    public function getUnProduit($ID)
    {
        $req = $this->connBd->prepare('SELECT idProduit,titre,description,prix,quantite,somme,nbVotes
         FROM produit WHERE idProduit=' . $ID );
        $req->execute();
        $leProduit = $req->fetch();
        $req->closeCursor();
        return $leProduit;
    }

    //update de la databse avec les paramètres pris en compte
    private function modifQtProduit($id, $qt)
    {  
        $req = $this->connBd->prepare('UPDATE produit SET quantite = quantite - '. $qt .
        ' WHERE idProduit=' . $id );
       $req->execute();
       $req->closeCursor();
    }

    //crée un boutton ajouter au panier
    public function afficherBouttonAjouterAuPanier($idProduit)
    {
        echo '<form method="get" action="/sources/produit-detail.php"><input type="hidden" name="id" value="'. $idProduit.'"><input type="image" src="/images/ajouter-panier.png" class="imgBouttonAjouterPanier"'. 
        '></form>';
    }

    //formhandler pour l'ajout d'un produit au panier, quantite peut être negatif
    public function ajouterQuantiteProduitPanier($idProduit, int $quantite, $quantiteStock)
    {
        if ($quantiteStock > 0) 
        {
            $liste = $_COOKIE['panier'];
            $trouve = false;
            //panier vide
            if (empty($liste))
            {
                if ($quantite > 0)
                {
                    $liste = $idProduit . '=' . $quantite . '.';
                }
            }
            else
            {   
                $trouve = false;
                $lesProduits = explode('.', $liste);
                $liste = '';
                for ($i=0; $i < count($lesProduits)-1; $i++) 
                { 
                    $lesProduits[$i] = explode('=',$lesProduits[$i]);
                    //produit trouvé?
                    if($lesProduits[$i][0] == $idProduit)
                    {
                        $trouve = true;
                        $nbFinal = intval($lesProduits[$i][1])+$quantite;
                        //si la quantite max est plus grande que ce qu'il reste en stock.
                        if ($nbFinal > $quantiteStock) 
                        {
                            $nbFinal = $quantiteStock;
                        }
                        $lesProduits[$i][1] = $nbFinal;
                        //le nb dans le panier est 0 ou moins
                        if ($nbFinal< 1)
                        {
                            $lesProduits[$i] = '';
                        }
                        else
                        {
                            $reEntree = $lesProduits[$i][0] . '=' . $lesProduits[$i][1];
                            $liste .= $reEntree . '.';
                        }
                    }
                    else
                    {
                        $reEntree = $lesProduits[$i][0] . '=' . $lesProduits[$i][1];
                        $liste .= $reEntree . '.';
                    }
                }
                if (!$trouve) 
                {
                    $liste .= $idProduit . '='. $quantite . '.';
                }
            }
            setrawcookie('panier', $liste, time()+60*60*24, '/');
            $_COOKIE['panier'] = $liste;
        }

    }

    /*  retourne un array de format
        liste[i][ID,Quantite]
    */
    public function GetElemsPanier($liste)
    {
        $items = explode('.', $liste);
        for ($i=0; $i < count($items)-1; $i++) 
        {
            $items[$i] = explode('=', $items[$i]);
            $items[$i][0] = $this->getUnProduit($items[$i][0]);
        }
        return $items;
    }

    public function RetirerDuPanier($idProduit)
    {
        if(strstr($_COOKIE['panier'],$idProduit) != FALSE)
        {
            $retour = '';
            $lesProduits = explode('.', $_COOKIE['panier']);
            $trouve = FALSE;
            for ($i=0; $i < count($lesProduits)-1 && $trouve == FALSE; $i++)
            { 
                //trouve produit
                if (explode('=',$lesProduits[$i])[0] == $idProduit)
                {
                    $lesProduits[$i] = '';
                }
                else
                {
                    $retour .= $lesProduits[$i] . '.';
                }
            }
            setrawcookie('panier', $retour, time()+60*60*24, '/');
            $_COOKIE['panier'] = $retour;
        }
    }

    //update la bd et réinitialise les cookies
    public function Caisse()
    {
        $liste = explode('.', $_COOKIE['panier']);
        for ($i=0; $i < count($liste)-1 ; $i++) 
        { 
            $liste[$i] = explode('=', $liste[$i]);
            $this->modifQtProduit($liste[$i][0], $liste[$i][1]);
        }
        $_COOKIE['panier'] = '';
        setrawcookie('panier', '', 1, '/');
    }


    //Retourne un array de produits trouvés grâce au $key, si rien n'est trouvé renvoie FALSE
    public function RechercheProduit($key, $nbOffset, $orderBy)
    {
        $listeRetour = [];
        //traitement du param
        $treatedKey = str_replace(' ', ',', $key);
        $explodedKey = explode(',', $treatedKey);

        $streReq = 'SELECT idProduit,titre,description,prix,quantite,somme,nbVotes ' .
        'FROM produit ';
        
        if (!empty($explodedKey))
        {
            $streReq .= "WHERE ";
            for ($i=0; $i < count($explodedKey); $i++) { 
                if ($explodedKey[$i] != '') 
                {
                    $streReq .= "titre LIKE '%" .$explodedKey[$i]. "%' OR ";
                    $streReq .= "idProduit= '" .$explodedKey[$i]. "' OR ";
                    $streReq .= "description LIKE '%" .$explodedKey[$i] . "%' OR ";
                }
            }
            //enlève le dernier OR
            $streReq = substr($streReq, 0, -3);
        }
        
        //ORDER BY PRIX
        switch ($orderBy) {
            case "desc":
                $streReq .= 'ORDER BY prix DESC ';
                break;
            case "asc":
                $streReq .= 'ORDER BY prix ';
                break;
            default:
                break;
        }
        //PAGE
        $streReq .= 'LIMIT 10 OFFSET ' . $nbOffset * 10;
        $req = $this->connBd->prepare($streReq);
        $req->execute();

        $listeRetour = $req->fetchAll();
        $req->closeCursor();
    
        if (empty($listeRetour)) 
        {
            $listeRetour = FALSE;
        }
        return $listeRetour;
    }
}