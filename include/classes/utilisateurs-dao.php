<?php
class UtilisateurDAO{
    private $connexionBD;

    //constructeur
    public function __construct(PDO $connexionBD)
    {
        $this->connexionBD = $connexionBD;
    }

    //Création d'un utilisateur
    public function creerUtilisateur(
        $courriel, 
        $mdp, 
        $nom = "",
        $adresse
        ) {
        return array(
            'courriel' => $courriel,
            'MDP' => hash('sha512', $mdp),
            'nom' => $nom,
            'IDUtil' => null,
            'adresse' => $adresse,
            'EstAdmin' => false
        );
        $req->closeCursor();
    }

    //Ajout de l'utilisateur à la BD
    public function ajoutUtilisateurBD($nom, $mail, $mdp, $adresse){
    {
        $req = $this->connexionBD->prepare(
            'INSERT INTO utilisateur(nom, courriel, mdp, adressePostale, EstAdministrateur)
                VALUES(:nom, :courriel, :mdp, :adresse, :EstAdministrateur)'
        );
        $req->execute(
            array(
                'nom' => $nom,
                'courriel' => $mail,
                'mdp' => hash('sha512', $mdp),
                'adresse' => $adresse,
                'EstAdministrateur' => false
            )
        );
        $req->closeCursor();
    }
    }

    //Vérifier l'existance d'un utilisateur
    public function verificationUtilisateur($util)
    {
        $req = $this->connexionBD->prepare('SELECT * FROM utilisateur WHERE Courriel = :courriel');
        $req->execute(array('courriel' => $util['courriel']));

        $infoUtil = $req->fetch();
        
        if(isset($infoUtil['courriel'])){
            //TODO Même avec un bon identifian le code se rend jamais ici
            if(!is_null($infoUtil['courriel'])){
                if($infoUtil['MDP'] == $util['MDP']){
                    //connexion OK
                    $utilisateur['IDUtil'] = $infoUtil['IDUtil'];
                    $utilisateur['EstAdmin'] = $infoUtil['EstAdmin'];
                    $utilisateur['nom'] = $infoUtil['nom'];
                    $req->closeCursor();
                    return true;
                }else{
                  //mdp error   
                }
            }
        }else{
            //email error
        }
        $req->closeCursor();
        return false;
    }

    //Vérification si le courriel est déjà utilisé pour un autre compte
    //Utilisé lors de la création d'un compte
    public function UtilisateurExiste($email)
    {
        $req = $this->connexionBD->prepare('SELECT * FROM utilisateur WHERE courriel ="'. $email.'"');
        $req->execute();

        $infoUtil = $req->fetchAll();
        $req->closeCursor();

        if (isset($infoUtil[0]) && !empty($infoUtil[0])) {
            return true;
        }
        else return false;
    }
}
?>