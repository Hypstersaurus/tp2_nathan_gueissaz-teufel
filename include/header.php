<header>
    <?php
        //initialisation des cookies
        $cookiePanierSet = false;

        require_once  $_SERVER['DOCUMENT_ROOT'] . '/include/cookies.php';

        //header html de documents
        echo '<!DOCTYPE html>';
        echo'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">';
        echo'<head>';
        echo'<title>Index</title>';
        echo'<meta charset="utf-8" />';
        echo'<link rel="stylesheet" type="text/css" href="/css/styles.css?'.time().'" />';
        echo '<link rel="icon" href="/images/placeholder.png">';
        echo'</head><div class="header"><ul class="listeHeader">';

        //affichage liens et infos compte
        if(isset($_SESSION['nom']))
        {
            echo '<li>Compte: ' . ($_SESSION['nom']) . '</li></a>';
            echo '<a href="\sources\deconnexion.php"><li>Déconnexion</li></a>';
            $admin = true;
        }
        else
        {    
            echo '<a href="/sources/authentification.php"><li>Connexion</li></a>';
            echo '<a href="/sources/creationcompte.php"><li>S\'enregistrer</li></a>';
            $admin = false;
        }
        
        //recherche
        echo '<a href="/sources/recherche.php"><li>Rechercher Produit</li></a></ul>';
        
        //Affichage panier
        if ($cookiePanierSet) 
        {
            $nbPanier = 0;
        }
        else  $nbPanier = substr_count($_COOKIE['panier'],'.');
        if ($nbPanier > 9) 
        {
            $nbPanier = "+";
        }
        echo '<div class="logo"><a href="\index.php"><img title="Retour à la page d\'accueil" src="\images\logo.png" width=100 lenght=100></a><br><div>';

        //inclusion des fichiers necessaires au php
        require_once $_SERVER['DOCUMENT_ROOT'] . '/include/classes/produits.php';

        //connexion BD
        $connBD = CreerConnexion();
    ?>
</header>

<!--Bouton récents-->
    <div id="divAffichageComplet" hidden></div>
    <img src= "/images/timelapse.png" title="Produits récemment consultés" id="imgRecents">

<!--Bouton panier-->
    <div id="divMenuPanier" style="display:none"></div>
    <div id="pardessusPanier" title="Panier"></div>
    <div id="panier" hidden>
    <p class="chiffreLogoPanier" id="nbLogoPanier"><?php echo $nbPanier?></p>
    <img src="\images\panier.png" id="logoPanier"></div>
    </div>
    <noscript>
    <?php echo '<a title="Panier" href="/sources/mon-panier.php"><p class="chiffreLogoPanier">'. 
    $nbPanier .'</p><img src="\images\panier.png" id="logoPanier"></a></div>'; ?>
    </noscript>

<!--JAVASCRIPT-->
    <script defer="defer" type="text/javascript" src="/js/utils-cookies.js"></script>
    <script defer="defer" type="text/javascript" src="/js/utils-dom.js"></script>
    <script defer="defer" type="text/javascript" src="/js/utils-ajax.js"></script>
    <script defer="defer" type="text/javascript" src="/js/produits-recents.js"></script>
    <script defer="defer" type="text/javascript" src="/js/panier.js"></script>