<?php 
    //inclusion cookies
    require_once 'include/cookies.php';
    //Inclusion header
    echo '<body>';
    require 'include/header.php';
    //creation objet de gestion de produits
    $produitDAO = new ProduitsDAO($connBD);
?>
<script defer="defer" type="text/javascript" src="/js/utils-dom.js"></script>
<script defer="defer" type="text/javascript" src="/js/utils-ajax.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script defer="defer" type="text/javascript" src="/js/visionneuse.js"></script>
    <h1 class="titreIndex">CryptoMarketPlace.com</h1>
    <br>
    <div class="contenantDeContenantsIndex" id="contenantGeneral">
        <!--Contenant des 5 produits en plus grande quantité-->
        <div class="contenantProduitsIndex" id="divQte">
        <h2 class="contenuIndex">Top 5 produits en plus grande quantité</h2>
            <?php

                $listeQt = $produitDAO->listerCinqPlusQuantite();

                //Vérification de l'image de chaque produit
                $i = 0;
                foreach ($listeQt as $produit)
                {
                    $imgSrc[$i]= "/images/produits/38.jpg";
                    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $imgSrc[$i])) 
                    {
                        $imgSrc[$i]= "/images/placeholder.png";
                    }
                    $i++;
                }

                $i = 0;
                foreach ($listeQt as $produit) {
                    echo '<a class="lienImage" href="sources/produit-detail.php/?id='. $produit["idProduit"] . '">'.
                    '<div class=contenuProduitIndex><img src="'.$imgSrc[$i].'" class=imgProduitIndex><ul class="contenuIndex"><li> ID: ' .
                     $produit["idProduit"] . " </li> <li> Produit: " . $produit["titre"] . ' </li> <li> Quantité: ' .
                     $produit["quantite"] . '</li>';
                     if ($admin)
                     {
                         echo  '<a href= "sources/edition-produit.php?/id='. $produit["idProduit"] . '">Modifier le produit</a>';
                     }
                     echo  '</ul></div></a>';
                     $i++;
                }
            ?>
        </div>
        <!--Contenant des 5 produits ayant la somme la plus élevée-->
        <div class="contenantProduitsIndex" id="divPop">
        <h2 class="contenuIndex">Top 5 produits les plus appréciés</h2>
                <?php

                    $listeQt = $produitDAO->listerCinqPlusNote();

                    //Vérification de l'image de chaque produit
                    $i = 0;
                    foreach ($listeQt as $produit)
                    {
                        $imgSrc[$i]= "/images/produits/38.jpg";
                        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $imgSrc[$i])) 
                        {
                            $imgSrc[$i]= "/images/placeholder.png";
                        }
                        $i++;
                    }
                    $i = 0;
                    foreach ($listeQt as $produit) {
                        echo '<a class="lienImage" href="sources/produit-detail.php/?id='. $produit["idProduit"] . '">'.
                        '<div class=contenuProduitIndex><img src="'.$imgSrc[$i].'" class=imgProduitIndex><ul class="contenuIndex"><li> ID: ' .
                         $produit["idProduit"] . " </li> <li> Produit: " . $produit["titre"] . ' </li> <li> Quantité: ' .
                         $produit["quantite"] . '</li>';
                         if ($admin)
                         {
                             echo  '<a href= "sources/edition-produit.php/?id='. $produit["idProduit"] . '">Modifier le produit</a>';
                         }
                         echo  '</ul></div></a>';
                         $i++;
                    }
                ?>
        </div>
    </div>
<?php require('include/footer.html');?>
</body>

</html>